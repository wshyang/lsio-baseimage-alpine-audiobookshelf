FROM advplyr/audiobookshelf:latest AS build
FROM ghcr.io/linuxserver/baseimage-alpine:3.15

RUN \
  mkdir /audiobooks || \
  mkdir /config || \
  mkdir /metadata || \
  mkdir -p /app/audiobookshelf-tmp || \
  apk update && \
  apk add --no-cache --virtual=build-dependencies npm nodejs && \
  apk add --no-cache --update ffmpeg

WORKDIR /app/audiobookshelf-tmp
COPY --from=build client client
COPY --from=build server server
COPY --from=build index.js index.js
COPY --from=build package-lock.json package-lock.json
COPY --from=build package.json package.json
RUN npm ci --production

COPY root/ /

VOLUME ["/audiobooks"]
VOLUME ["/config"]
VOLUME ["/metadata"]

EXPOSE 80